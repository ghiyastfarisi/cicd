package main

import (
	"fmt"
	"log"

	"github.com/gofiber/fiber/v2"
)

func main() {
	app := fiber.New()

	app.Get("/ping", func(c *fiber.Ctx) error {
		return c.SendString("pong 🏓")
	})

	app.Get("/health", func(c *fiber.Ctx) error {
		return c.JSON(map[string]interface{}{
			"message": "OK",
		})
	})

	fmt.Println("|====================|")
	fmt.Println("| running cicd/goapp |")
	fmt.Println("|====================|")

	if err := app.Listen(":8000"); err != nil {
		log.Fatal(err)
	}
}
